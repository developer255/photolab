var express = require('express');
var router = express.Router();
const authorize = require('../_helpers/authorize');
var subcategoriesController = require('../src/controllers/subcategory')
const Role = require('../_helpers/role');

router.post('/create', authorize(), subcategoriesController.createSubCategory)
router.post('/list', authorize(), subcategoriesController.getAllSubCategories)
router.put('/update', authorize(), subcategoriesController.updateSubCategory)
router.get('/details/:id', authorize(), subcategoriesController.getSubCategoryById)
router.get('/detailsByParent/:parentid', authorize(), subcategoriesController.getSubCategoryByParentId)
router.delete('/delete/:id', authorize(), subcategoriesController.removeSubCategory)
router.put('/reactivate/:id', authorize(), subcategoriesController.reactivateSubCategory)

module.exports = router;