var express = require('express');
var router = express.Router();
const authorize = require('../_helpers/authorize');
var rolesController = require('../src/controllers/role')
const Role = require('../_helpers/role');

router.post('/create', authorize(), rolesController.createRole)
router.post('/list', authorize(), rolesController.getAllRoles)
router.put('/update', authorize(), rolesController.updateRole)
router.get('/details/:id', authorize(), rolesController.getRoleById)
router.delete('/delete/:id', authorize(), rolesController.removeRole)
router.put('/reactivate/:id', authorize(), rolesController.reactivateRole)

module.exports = router;