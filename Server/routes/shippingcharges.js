var express = require('express');
var router = express.Router();
const authorize = require('../_helpers/authorize');
var shippingChargesController = require('../src/controllers/shippingcharges')
const Role = require('../_helpers/role');

router.post('/create', authorize(), shippingChargesController.createShippingCharges)
router.post('/list', authorize(), shippingChargesController.getAllShippingCharges)
router.put('/update', authorize(), shippingChargesController.updateShippingCharges)
router.get('/details/:id', authorize(), shippingChargesController.getShippingChargesById)
router.delete('/delete/:id', authorize(), shippingChargesController.removeShippingCharges)
router.put('/reactivate/:id', authorize(), shippingChargesController.reactivateShippingCharges)

module.exports = router;