var express = require('express');
var router = express.Router();
const authorize = require('../_helpers/authorize');
var customerController = require('../src/controllers/customer')
const Role = require('../_helpers/role');
var multer = require('multer');

// Customer - Create , Update , Delete, user list, user by id
var customerImgStorage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/images/customer');
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    }
  });
  
  var uploadCustomerImage = multer({
      storage: customerImgStorage
  }).single('fileInput');

router.post('/create', authorize(), uploadCustomerImage, customerController.createCustomer)
router.put('/update', authorize(), customerController.updateCustomer)
router.delete('/delete/:id', authorize(), customerController.deleteCustomer)
router.put('/reactivate/:id', authorize(), customerController.reactivateCustomer)
router.post('/list', authorize(), customerController.getCustomers)
router.get('/details/:id', authorize(), customerController.getCustomerById)
router.put('/updateImage/:id', authorize(), uploadCustomerImage, customerController.updateUserImage)

router.post('/addAddress', authorize(), customerController.addAddress)
router.delete('/removeAddress/:id', authorize(), customerController.removeAddress)
router.put('/updateAddress', authorize(), customerController.updateAddress)
router.get('/address/:id', authorize(), customerController.getAddress)
router.post('/changeDefaultAddress', authorize(), customerController.changeDefaultAddress)

module.exports = router;