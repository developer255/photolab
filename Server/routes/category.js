var express = require('express');
var router = express.Router();
const authorize = require('../_helpers/authorize');
var categoriesController = require('../src/controllers/category')
const Role = require('../_helpers/role');

router.post('/create', authorize(), categoriesController.createCategory)
router.post('/list', authorize(), categoriesController.getAllCategories)
router.put('/update', authorize(), categoriesController.updateCategory)
router.get('/details/:id', authorize(), categoriesController.getCategoryById)
router.delete('/delete/:id', authorize(), categoriesController.removeCategory)
router.put('/reactivate/:id', authorize(), categoriesController.reactivateCategory)

module.exports = router;