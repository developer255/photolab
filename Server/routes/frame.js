var express = require('express');
var router = express.Router();
const authorize = require('../_helpers/authorize');
var frameController = require('../src/controllers/frame')
const Role = require('../_helpers/role');
var multer = require('multer');

var frameImgStorage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/images/frame');
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    }
  });
  
  var uploadFrameImage = multer({
      storage: frameImgStorage
  }).single('fileInput');

router.post('/create', authorize(), uploadFrameImage, frameController.createFrame)
router.post('/list', authorize(), frameController.getFrames)
router.put('/update', authorize(), frameController.updateFrame)
router.get('/details/:id', authorize(), frameController.getFrameById)
router.delete('/delete/:id', authorize(), frameController.removeFrame)
router.put('/reactivate/:id', authorize(), frameController.reactivateFrame)
router.put('/updateImage/:id', authorize(), uploadFrameImage, frameController.updateImage)

router.post('/review', authorize(), frameController.createReview)
router.put('/updatereview', authorize(), frameController.updateReview)
router.delete('/review/:id', authorize(), frameController.deleteReview)

module.exports = router;