var express = require('express');
var router = express.Router();

const authorize = require('../_helpers/authorize');
const bodyParser = require('body-parser');


const usrs = [
    { id: 1, username: 'admin', password: 'admin', firstname: 'Bhupesh', lastname: 'Bhupesh', contactno: '9898652356', email: 'bhupesh@test.com', role: 'Admin' },
    { id: 2, username: 'customer', password: 'customer', firstname: 'Mithun', lastname: 'Mithun', contactno: '9898652356', email: 'mithun@test.com', role: 'Customer' },
    { id: 3, username: 'delivery', password: 'delivery', firstname: 'Tanush', lastname: 'Tanush', contactno: '9898652356', email: 'tanush@test.com', role: 'Delivery' }
];

router.get('/getusers', authorize(), function(req, res, next) {
    res.send(usrs)
});

module.exports = router;