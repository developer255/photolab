var express = require('express');
var router = express.Router();
const authorize = require('../_helpers/authorize');
var orderStatusController = require('../src/controllers/orderstatus')
const Role = require('../_helpers/role');

router.post('/create', authorize(), orderStatusController.createOrderStatus)
router.post('/list', authorize(), orderStatusController.getAllOrderStatus)
router.put('/update', authorize(), orderStatusController.updateOrderStatus)
router.get('/details/:id', authorize(), orderStatusController.getOrderStatusById)
router.delete('/delete/:id', authorize(), orderStatusController.removeOrderStatus)
router.put('/reactivate/:id', authorize(), orderStatusController.reactivateOrderStatus)

module.exports = router;