var express = require('express');
var router = express.Router();
const authorize = require('../_helpers/authorize');
var orderController = require('../src/controllers/order')
const Role = require('../_helpers/role');
var multer = require('multer');

var orderImgStorage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/images/order');
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    }
  });
  
  var uploadOrderImage = multer({
      storage: orderImgStorage
  }).fields([{
        name:'img_1',
        maxCount:1
    },{
        name:'img_2', 
        maxCount:1
    }] 
)

router.post('/create', authorize(), uploadOrderImage, orderController.createOrder)
router.put('/orderPayment', authorize(), orderController.orderPayment)
router.post('/list', authorize(), orderController.getOrders)
// router.put('/update', authorize(), orderController.updateOrder)
router.get('/details/:id', authorize(), orderController.getOrderById)
router.post('/delete', authorize(), orderController.removeOrder)
router.post('/statusUpdate', authorize(), orderController.updateOrderStatus)
router.get('/:id/:customerId', authorize(), orderController.getCustomerOrderById)

module.exports = router;