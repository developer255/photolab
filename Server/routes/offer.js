var express = require('express');
var router = express.Router();
const authorize = require('../_helpers/authorize');
var offerController = require('../src/controllers/offer')
const Role = require('../_helpers/role');
var multer = require('multer');

var offerImgStorage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/images/offer');
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    }
  });
  
  var uploadOfferImage = multer({
      storage: offerImgStorage
  }).single('fileInput');
  
router.post('/create', authorize(), uploadOfferImage, offerController.createOffer)
router.post('/list', authorize(), offerController.getAllOffers)
router.put('/update', authorize(), offerController.updateOffer)
router.get('/details/:id', authorize(), offerController.getOfferById)
router.delete('/delete/:id', authorize(), offerController.removeOffer)
router.put('/updateImage/:id', authorize(), uploadOfferImage, offerController.updateImage)
router.post('/validate', authorize(), offerController.validateOfferCode)

module.exports = router;