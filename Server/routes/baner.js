var express = require('express');
var router = express.Router();
const authorize = require('../_helpers/authorize');
var banerController = require('../src/controllers/baner')
const Role = require('../_helpers/role');
var multer = require('multer');

var banerImgStorage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/images/baner');
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    }
});

var uploadBanerImage = multer({
    storage: banerImgStorage
}).single('fileInput');

router.post('/create', authorize(), uploadBanerImage, banerController.createBaner)
router.delete('/delete/:id', authorize(), banerController.removeBaner)
router.get('/list', authorize(), banerController.getBaners)

module.exports = router;