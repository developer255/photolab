var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
var logger = require('morgan');
const authorize = require('./_helpers/authorize')


var pool = require('./_helpers/config');
var _ = require('underscore');


var indexRouter = require('./routes/index');
const roleRouter = require('./routes/role');
const customerRouter = require('./routes/customer');
const categoryRouter = require('./routes/category');
const subcategoryRouter = require('./routes/subcategory');
const banerRouter = require('./routes/baner');
const frameRouter = require('./routes/frame');
const orderStatusRouter = require('./routes/orderstatus');
const shippingChargesRouter = require('./routes/shippingcharges');
const offerRouter = require('./routes/offer');
const orderRouter = require('./routes/order');

var app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");
    next();
});
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false }));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/customerImg', express.static('./public/images/customer'));
app.use('/banerImg', express.static('./public/images/baner'));
app.use('/frameImg', express.static('./public/images/frame'));
app.use('/offerImg', express.static('./public/images/offer'));
app.use('/orderImg', express.static('./public/images/order'));

app.use('/', indexRouter);
app.use('/authorization', require('./src/authorization/authorize.controller'));
app.use('/role', roleRouter);
app.use('/customer', customerRouter);
app.use('/category', categoryRouter);
app.use('/subcategory', subcategoryRouter);
app.use('/baner', banerRouter);
app.use('/frame', frameRouter);
app.use('/orderstatus', orderStatusRouter);
app.use('/shippingCharges', shippingChargesRouter);
app.use('/offer', offerRouter);
app.use('/order', orderRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;