# framecartbackend
This is the framecart backend repo for APIs and dashboard


After check out execute following command

npm install 

************ Use following command to run application *************

DEBUG=framekartserver:* npm start

OR 

npm start


************ Database Installation and configuration **************

    After installation of psql follow below steps:

    1) Connect to db
        > psql postgres
        
    2) Check information of connected db by which user
        postgres=# \conninfo
        
    3) Create new role for your use 
        postgres=# CREATE USER root WITH PASSWORD 'password';
        
    4) Create database: 
        postgres=# CREATE DATABASE framekart;
        
    5) Grant priviliges :
        postgres=# GRANT ALL PRIVILEGES ON DATABASE framekart to root;        
        
    6) Exit from current role
    postgres=# \q
    
    7) connect to db using user
        psql -U root -h 127.0.0.1 framekart
        
    8) Some importatan commands:
        \q 	>  Exit psql connection
        \c 	> Connect to a new database
        \dt 	> List all tables
        \du 	> List all roles
        \list 	> List databases
