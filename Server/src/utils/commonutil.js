const pool = require('../../config');
var mailer = require("nodemailer");
var moment = require('moment');
var _ = require('underscore');

function randomString() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    console.log("randomstring: ", randomstring);
    return randomstring;
}

const sendMail = async (orderDetails, to) =>{
   var smtpTransport =  mailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    auth: {
        user: '<<mailId>>',
        pass: '<<pwd>>'
    } 
   });

    console.log(" Reciever mail id ", to);    
   
    var mail = {
        from: "<<mailId>>",
        to: to,
        subject: "Welcome mail",
        generateTextFromHTML: true,
        html: "Thanks." 
    }

    
    
    var tets = await smtpTransport.sendMail(mail, function(error, response){
        if(error){
            console.log("throwing error : ", error);
        }else{
            console.log("Message sent: ", mail.html);
            return true;
        }
    
        smtpTransport.close();
    });

}

module.exports = {
    randomString,
    sendMail
};