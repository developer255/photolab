-- Role Master Table
CREATE TABLE role (
    id serial PRIMARY KEY, 
    role_name VARCHAR (50) UNIQUE NOT NULL, -- This is role name. it will be unique
    created_on TIMESTAMP NOT NULL, 
    updated_on TIMESTAMP,
    is_active BOOLEAN DEFAULT true -- We can deactivate role is not any customer assign to it
);

INSERT INTO role(role_name, created_on)
VALUES ('Admin', now());
INSERT INTO role(role_name, created_on)
VALUES ('User', now());

-- customer Maste Table
CREATE TYPE gender AS ENUM ('male', 'female', 'other');
CREATE TABLE customer(
    id serial PRIMARY KEY,
    full_name VARCHAR (200) NOT NULL,
    email VARCHAR (150) UNIQUE,
    date_of_birth DATE,
    mobile_no VARCHAR (30) UNIQUE NOT NULL, -- If we allow to otp base login then it need to be unique 
    gender gender,
    is_active BOOLEAN DEFAULT true,
    user_name VARCHAR (50) UNIQUE NOT NULL, 
    password VARCHAR (400) NOT NULL,
    updated_on TIMESTAMP,        
    created_on TIMESTAMP NOT NULL,
    last_login TIMESTAMP,        
    customer_refer_code VARCHAR (200), -- This for refral program if we use refre functionality then ever customer need  it's own refre code    
    CONSTRAINT refer_code_unique UNIQUE (customer_refer_code)
);

-- customer address
CREATE TABLE customer_address (
    id serial PRIMARY KEY,
    customer_id INTEGER REFERENCES customer(id) NOT NULL,
    address VARCHAR (500) NOT NULL,
    city VARCHAR (40),
    state VARCHAR (40),
    country VARCHAR (40),
    land_mark VARCHAR (40),
    pincode INTEGER,
    address_type VARCHAR (40) DEFAULT 'home', -- 'home', 'billing', 'shipping'
    is_active BOOLEAN DEFAULT true,    
    is_default BOOLEAN DEFAULT false, -- when u select is default then this always populate as selected address
    mobile_no VARCHAR (30),
    created_on TIMESTAMP NOT NULL,
    updated_on TIMESTAMP
);

-- customer - Role Relation Table	
CREATE TABLE customer_role (
    customer_id INTEGER NOT NULL,
    role_id INTEGER NOT NULL,
    created_on TIMESTAMP NOT NULL,
    -- approve_date TIMESTAMP without time zone, -- this need to decide is we have to keep approval process to new customers
    PRIMARY KEY (customer_id, role_id),
    CONSTRAINT customer_role_role_id_fkey FOREIGN KEY (role_id)
        REFERENCES role (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT customer_role_customer_id_fkey FOREIGN KEY (customer_id)
        REFERENCES customer (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- Category Master Table
CREATE TABLE category (
    id serial PRIMARY KEY,
    name VARCHAR (255) UNIQUE NOT NULL,
    created_on TIMESTAMP NOT NULL,
    updated_on TIMESTAMP
);

-- Subcategory Master Table
CREATE TABLE sub_category (
    id serial PRIMARY KEY,
    name VARCHAR (255) UNIQUE NOT NULL,
    parent INTEGER REFERENCES category (id),
    created_on TIMESTAMP NOT NULL,
    updated_on TIMESTAMP
);


-- Item Master Table
CREATE TABLE frame (
    id serial PRIMARY KEY,
    frame_code VARCHAR (255),
    frame_name VARCHAR (255) NOT NULL,
    frame_desc VARCHAR (400),  
    frame_type VARCHAR (100), -- 8*8, 10*10 any one of this as type make it to not null
    category_id INTEGER REFERENCES category (id), 
    sub_category_id INTEGER REFERENCES sub_category (id),
    price DECIMAL(5,2), -- Price with 5 pricision and 2 scale ex. 12345.12 make it to not null
    created_on TIMESTAMP NOT NULL,
    is_active BOOLEAN DEFAULT true,
    updated_on TIMESTAMP
);

CREATE TABLE frame_review (
    id serial PRIMARY KEY,
    customer_id INTEGER REFERENCES customer(id) NOT NULL,
    frame_id INTEGER REFERENCES frame(id) NOT NULL,
    review VARCHAR (500) NOT NULL,  -- it means comment 
    rating INTEGER NOT NULL, -- rating will 1 or 2 or 3 or 4 or 5
    created_on TIMESTAMP NOT NULL,
    updated_on TIMESTAMP,
    CONSTRAINT customer_frame_unique UNIQUE (customer_id, frame_id)
);

-- Offer Master Table
CREATE TABLE offer (
    id serial PRIMARY KEY,
    offer_value INTEGER, -- value how discount will apply depend on type
    offer_type VARCHAR (50) , -- "percent" or "flat" or "points" or "coupon"
    offer_desc VARCHAR (255),
    offer_code VARCHAR (50) NOT NULL,
    created_on TIMESTAMP NOT NULL,
    is_active BOOLEAN DEFAULT true,
    from_date date NOT NULL,
    to_date date NOT NULL,
    updated_on TIMESTAMP
);

-- Entity wise Image Table
CREATE TABLE images (
    id serial PRIMARY KEY,
    entity_type VARCHAR (255) NOT NULL,  --  "Frame" or "offer" or "customer" or "customer_choose_image" or "Baner"
    entity_id INTEGER NOT NULL, -- "frame_id" or "Offer_id" or "customer_id" or "order_item_id" or "baner_id"
    img_name VARCHAR (255),
    img_path VARCHAR (255),
    created_on TIMESTAMP NOT NULL,
    updated_on TIMESTAMP
);

-- order_status Master Table
CREATE TABLE order_status (
    id serial PRIMARY KEY, 
    status_title VARCHAR (50) UNIQUE NOT NULL, 
    created_on TIMESTAMP NOT NULL, 
    updated_on TIMESTAMP,
    is_active BOOLEAN DEFAULT true 
);

CREATE TABLE shipping_charges (
    id serial PRIMARY KEY, 
    amount_slab VARCHAR (25) NOT NULL,  -- slab will be like 0-100, 101-1000, 1001- 2001
    charges decimal(5,2) NOT NULL,
    created_on TIMESTAMP NOT NULL, 
    updated_on TIMESTAMP,
    is_active BOOLEAN DEFAULT true 
);

-- order Master Table
CREATE TABLE orders (
    id serial PRIMARY KEY,
    customer_id INTEGER REFERENCES customer(id) NOT NULL,
    offer_id INTEGER REFERENCES offer(id),
    shipping_charges_id INTEGER REFERENCES shipping_charges(id),
    payment_mode VARCHAR (255), 
    payment_status VARCHAR (255), 
    transaction_num VARCHAR (255),    
    payment_date TIMESTAMP,
    order_status_id INTEGER REFERENCES order_status(id),
    total_amount decimal(12,2), 
    order_date TIMESTAMP NOT NULL,
    created_on TIMESTAMP NOT NULL,
    updated_on TIMESTAMP  
);


CREATE TABLE order_items (
    id serial PRIMARY KEY, -- This id will use for store choose image in images table
    order_id INTEGER REFERENCES orders(id) NOT NULL,
    frame_id INTEGER REFERENCES frame(id) NOT NULL,
    quantity INTEGER,
    price decimal(5,2),
    name_on_frame VARCHAR,
    delivery_date TIMESTAMP,
    occasion VARCHAR,
    updated_on TIMESTAMP
);

-- Baner Table
CREATE TABLE baner (
    id serial PRIMARY KEY, 
    title VARCHAR (100) NOT NULL, 
    baner_desc VARCHAR (255), 
    created_on TIMESTAMP NOT NULL, 
    updated_on TIMESTAMP,
    is_active BOOLEAN DEFAULT true 
);



-- Alter tables 
ALTER TABLE customer 
ADD COLUMN iv VARCHAR (400),
ADD COLUMN envkey VARCHAR (400),
ALTER COLUMN password drop not null;

ALTER TABLE frame
ALTER COLUMN frame_type SET not null,
DROP COLUMN price,
ADD COLUMN qty_price varchar ARRAY;

ALTER TABLE offer
ADD COLUMN point integer;

-- ALTER TABLE shipping_charges 
-- DROP COLUMN charges,
-- ADD COLUMN charges integer NOT NULL;

ALTER TABLE category
ADD COLUMN is_active BOOLEAN DEFAULT true;

ALTER TABLE sub_category
ADD COLUMN is_active BOOLEAN DEFAULT true;
