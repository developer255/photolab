var pool = require('../../_helpers/config');
const frameService = require('../services/frame.service');

async function createFrame(req, res, next) {  
  await frameService.createFrame(req, res, next);
}

async function getFrames(req, res, next) {  
  await frameService.getFrames(req, res, next);
}

async function getFrameById(req, res, next) {  
  await frameService.getFrameById(req, res, next);
}

async function removeFrame(req, res, next) {  
  await frameService.removeFrame(req, res, next);
}

async function updateImage(req, res, next) {  
  await frameService.updateImage(req, res, next);
}

async function updateFrame(req, res, next) {  
  await frameService.updateFrame(req, res, next);
}

async function reactivateFrame(req, res, next) {  
  await frameService.reactivateFrame(req, res, next);
}

async function createReview(req, res, next) {  
  await frameService.createReview(req, res, next);
}

async function updateReview(req, res, next) {  
  await frameService.updateReview(req, res, next);
}

async function deleteReview(req, res, next) {  
  await frameService.deleteReview(req, res, next);
}

module.exports = {
  createFrame,
  getFrames,
  getFrameById,
  removeFrame,
  updateImage,
  updateFrame,
  reactivateFrame,
  createReview,
  updateReview,
  deleteReview
}