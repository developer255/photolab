var pool = require('../../_helpers/config');
const roleService = require('../services/role.service');

async function createRole(req, res, next) {  
  await roleService.createRole(req, res, next);
}


async function getAllRoles(req, res, next) {  
  await roleService.getAllRoles(req, res, next);
}

async function getRoleById(req, res, next) {  
  await roleService.getRoleById(req, res, next);
}

async function updateRole(req, res, next) {  
  await roleService.updateRole(req, res, next);
}

async function removeRole(req, res, next) {  
  await roleService.removeRole(req, res, next);
}

async function reactivateRole(req, res, next) {  
  await roleService.reactivateRole(req, res, next);
}

module.exports = {
    createRole,
    getAllRoles,
    updateRole,
    getRoleById,
    removeRole,
    reactivateRole
}