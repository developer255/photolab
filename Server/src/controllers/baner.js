var pool = require('../../_helpers/config');
const banerService = require('../services/baner.service');

async function createBaner(req, res, next) {
    await banerService.createBaner(req, res, next);
}

async function getBaners(req, res, next) {
    await banerService.getBaners(req, res, next);
}

async function removeBaner(req, res, next) {
    await banerService.removeBaner(req, res, next);
}

module.exports = {
    createBaner,
    getBaners,
    removeBaner
}