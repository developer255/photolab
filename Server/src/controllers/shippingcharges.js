var pool = require('../../_helpers/config');
const shippingChargesService = require('../services/shippingcharges.service');

async function createShippingCharges(req, res, next) {  
  await shippingChargesService.createShippingCharges(req, res, next);
}


async function getAllShippingCharges(req, res, next) {  
  await shippingChargesService.getAllShippingCharges(req, res, next);
}

async function getShippingChargesById(req, res, next) {  
  await shippingChargesService.getShippingChargesById(req, res, next);
}

async function updateShippingCharges(req, res, next) {  
  await shippingChargesService.updateShippingCharges(req, res, next);
}

async function removeShippingCharges(req, res, next) {  
  await shippingChargesService.removeShippingCharges(req, res, next);
}

async function reactivateShippingCharges(req, res, next) {  
  await shippingChargesService.reactivateShippingCharges(req, res, next);
}

module.exports = {
    createShippingCharges,
    getAllShippingCharges,
    updateShippingCharges,
    getShippingChargesById,
    removeShippingCharges,
    reactivateShippingCharges
}