var pool = require('../../_helpers/config');
const orderStatusService = require('../services/orderstatus.service');

async function createOrderStatus(req, res, next) {  
  await orderStatusService.createOrderStatus(req, res, next);
}


async function getAllOrderStatus(req, res, next) {  
  await orderStatusService.getAllOrderStatus(req, res, next);
}

async function getOrderStatusById(req, res, next) {  
  await orderStatusService.getOrderStatusById(req, res, next);
}

async function updateOrderStatus(req, res, next) {  
  await orderStatusService.updateOrderStatus(req, res, next);
}

async function removeOrderStatus(req, res, next) {  
  await orderStatusService.removeOrderStatus(req, res, next);
}

async function reactivateOrderStatus(req, res, next) {  
  await orderStatusService.reactivateOrderStatus(req, res, next);
}

module.exports = {
    createOrderStatus,
    getAllOrderStatus,
    updateOrderStatus,
    getOrderStatusById,
    removeOrderStatus,
    reactivateOrderStatus
}