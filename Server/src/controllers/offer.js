var pool = require('../../_helpers/config');
const offerService = require('../services/offer.service');

async function createOffer(req, res, next) {  
  await offerService.createOffer(req, res, next);
}


async function getAllOffers(req, res, next) {  
  await offerService.getAllOffers(req, res, next);
}

async function getOfferById(req, res, next) {  
  await offerService.getOfferById(req, res, next);
}

async function updateOffer(req, res, next) {  
  await offerService.updateOffer(req, res, next);
}

async function removeOffer(req, res, next) {  
  await offerService.removeOffer(req, res, next);
}

async function updateImage(req, res, next) {  
  await offerService.updateImage(req, res, next);
}

async function validateOfferCode(req, res, next) {  
  await offerService.validateOfferCode(req, res, next);
}

module.exports = {
    createOffer,
    getAllOffers,
    updateOffer,
    getOfferById,
    removeOffer,
    updateImage,
    validateOfferCode
}