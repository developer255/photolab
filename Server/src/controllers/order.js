var pool = require('../../_helpers/config');
const orderService = require('../services/order.service');

async function createOrder(req, res, next) {  
  await orderService.createOrder(req, res, next);
}

async function getOrders(req, res, next) {  
  await orderService.getOrders(req, res, next);
}

async function getOrderById(req, res, next) {  
  await orderService.getOrderById(req, res, next);
}

async function removeOrder(req, res, next) {  
  await orderService.removeOrder(req, res, next);
}

async function updateOrder(req, res, next) {  
  await orderService.updateOrder(req, res, next);
}

async function updateOrderStatus(req, res, next) {  
  await orderService.updateOrderStatus(req, res, next);
}

async function orderPayment(req, res, next) {  
  await orderService.orderPayment(req, res, next);
}
async function getCustomerOrderById(req, res, next) {  
  await orderService.getCustomerOrderById(req, res, next);
}


module.exports = {
  createOrder,
  getOrders,
  getOrderById,
  removeOrder,
  updateOrder,
  updateOrderStatus,
  orderPayment,
  getCustomerOrderById
}