const categoryService = require('../services/category.service');

async function createCategory(req, res, next) {
    await categoryService.createCategory(req, res, next);
}

async function updateCategory(req, res, next) {
    await categoryService.updateCategory(req, res, next);
}

async function getAllCategories(req, res, next) {
    await categoryService.getAllCategories(req, res, next);
}

async function getCategoryById(req, res, next) {
    await categoryService.getCategoryById(req, res, next);
}

async function removeCategory(req, res, next) {
    await categoryService.removeCategory(req, res, next);
}

async function reactivateCategory(req, res, next) {
    await categoryService.reactivateCategory(req, res, next);
}

module.exports = {
    createCategory,
    updateCategory,
    getAllCategories,
    getCategoryById,
    removeCategory,
    reactivateCategory
}