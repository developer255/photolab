const subcategoryService = require('../services/subcategory.service');

async function createSubCategory(req, res, next) {
    await subcategoryService.createSubCategory(req, res, next);
}

async function updateSubCategory(req, res, next) {
    await subcategoryService.updateSubCategory(req, res, next);
}

async function getAllSubCategories(req, res, next) {
    await subcategoryService.getAllSubCategories(req, res, next);
}

async function getSubCategoryById(req, res, next) {
    await subcategoryService.getSubCategoryById(req, res, next);
}

async function getSubCategoryByParentId(req, res, next) {
    await subcategoryService.getSubCategoryByParentId(req, res, next);
}

async function removeSubCategory(req, res, next) {
    await subcategoryService.removeSubCategory(req, res, next);
}

async function reactivateSubCategory(req, res, next) {
    await subcategoryService.reactivateSubCategory(req, res, next);
}



module.exports = {
    createSubCategory,
    updateSubCategory,
    getAllSubCategories,
    getSubCategoryById,
    getSubCategoryByParentId,
    removeSubCategory,
    reactivateSubCategory
}