var pool = require('../../_helpers/config');
const customerService = require('../services/customer.service');

async function createCustomer(req, res, next) {  
  await customerService.createCustomer(req, res, next);
}

async function updateCustomer(req, res, next) {  
  await customerService.updateCustomer(req, res, next);
}

async function deleteCustomer(req, res, next) {  
  await customerService.deleteCustomer(req, res, next);
}

async function reactivateCustomer(req, res, next) {  
  await customerService.reactivateCustomer(req, res, next);
}

async function getCustomers(req, res, next) {  
  await customerService.getCustomers(req, res, next);
}

async function getCustomerById(req, res, next) {  
  await customerService.getCustomerById(req, res, next);
}

async function updateUserImage(req, res, next) {  
  await customerService.updateUserImage(req, res, next);
}


async function addAddress(req, res, next) {  
  await customerService.addAddress(req, res, next);
}

async function getAddress(req, res, next) {  
  await customerService.getAddress(req, res, next);
}

async function updateAddress(req, res, next) {  
  await customerService.updateAddress(req, res, next);
}

async function removeAddress(req, res, next) {  
  await customerService.removeAddress(req, res, next);
}

async function changeDefaultAddress(req, res, next) {  
  await customerService.changeDefaultAddress(req, res, next);
}



module.exports = {
  createCustomer,
  updateCustomer,
  deleteCustomer,
  reactivateCustomer,
  getCustomers,
  getCustomerById,
  updateUserImage,
  addAddress,
  getAddress,
  updateAddress,
  removeAddress,
  changeDefaultAddress
}