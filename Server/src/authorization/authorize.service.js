const config = require('config.json');
var pool = require('../../_helpers/config');
var _ = require('underscore');
var moment = require('moment');
const jwt = require('jsonwebtoken');
const Role = require('../../_helpers/role');
var errorHandler = require('../../_helpers/error-handler');
const crypto = require('crypto');
const algorithm = 'aes-256-cbc';

async function authenticate(req, res, next) {
    // const user = usrs.find(u => u.username === username && u.password === password);
    // if (user) {
    //     const token = jwt.sign({ sub: user.id, role: user.role }, 'THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING'); // config.secret);
    //     const { password, ...userWithoutPassword } = user;
    //     return {
    //         ...userWithoutPassword,
    //         token
    //     };
    // }


    const {username, password} = req.body

    pool.query(`SELECT c.*, r.role_name, r.id "roleId" FROM customer c 
                inner join customer_role cr on c.id = cr.customer_id
                inner join role r on cr.role_id = r.id
                WHERE c.user_name = $1`, [username])
    .then(function (results) {
        if(_.isEmpty(results.rows)){
            res.status(500).json({status: "Failure", message:"Invalid Username"})
        }

        if(results.rows[0]){
            var encryptString = decrypt({encryptedData: results.rows[0].password, iv: results.rows[0].iv, key: results.rows[0].envkey});
            if(password === encryptString && results.rows.length !== 0){
                var userDetails = {}

                var userObj = results.rows;
                _.each(userObj, function(user){
                    userDetails = {
                        "id": user.id,
                        "name": user.full_name,
                        "userName": user.user_name,
                        "email": user.email,
                        "date_of_birth": user.date_of_birth ? moment(user.date_of_birth).format('YYYY-MM-DD') : null,
                        "mobile_no": user.mobile_no,
                        "gender": user.gender,
                        "role":{
                            "id": user.roleId,
                            "name": user.role_name
                        }
                    }
                });
                const token = jwt.sign({ sub: userDetails.id, role: userDetails.role_name }, 'THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING'); // config.secret);
                userDetails.token = token;
                res.status(200).json({status: "success", userDetails:userDetails});
            } else {
                res.status(500).json({status: "failure", message:"Password is incorrect."})
                return;
            }
        }
    })
    .catch(function (err) {
        return errorHandler(err, req, res, next);
    });
}

function decrypt(text) {
    let iv = Buffer.from(text.iv, 'hex');
    let envkey = Buffer.from(text.key, 'hex');
    let encryptedText = Buffer.from(text.encryptedData, 'hex');
    let decipher = crypto.createDecipheriv(algorithm, Buffer.from(envkey), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

module.exports = {
    authenticate
};