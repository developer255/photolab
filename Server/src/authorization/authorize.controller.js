const express = require('express');
const router = express.Router();
const userService = require('./authorize.service');

// routesauthorize
router.post('/authenticate', authenticate); // public route

function authenticate(req, res, next) {
    userService.authenticate(req, res, next);
}

module.exports = router;