var pool = require('../../_helpers/config');
var _ = require('underscore');
var moment = require('moment');


// function to create subcategory
async function createSubCategory(req, res, next) {
    const { name, parent } = req.body
    pool.query('INSERT INTO sub_category (name,parent,created_on) VALUES ($1,$2,now()) RETURNING id', [name, parent])
        .then(function(result) {
            const { subcategory } = result.rows[0].id;
            res.status(200).json({
                status: 'success',
                message: 'Inserted one subcategory with id ' + result.rows[0].id
            });
        })
        .catch(function(err) {
            return next(err);
        });
}

// function to update subcategory
async function updateSubCategory(req, res, next) {
    const { id, name } = req.body
    pool.query('UPDATE sub_category SET name = $2, updated_on = now() WHERE id = $1', [id, name])
        .then(function(result) {
            res.status(200).json({
                status: 'success',
                message: 'SubCategory has been updated successfully!!!'
            });
        })
        .catch(function(err) {
            return next(err);
        });
}

// function to get sub category list
async function getAllSubCategories(req, res, next) {
    const { start, limit, status } = req.body
    var query = `SELECT sc.*, count(sc.*) OVER() AS full_count from sub_category sc ORDER BY id ASC`;
    
    if (_.has(req.body, "status")) {
        query = `SELECT sc.*, count(sc.*) OVER() AS full_count from sub_category sc where sc.is_active = ${status} ORDER BY id ASC`;        
    }

    if (_.has(req.body, "start") && _.has(req.body, "limit")) {
        query = `${query} OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`
    }

    pool.query(query)
        .then(function(result) {
            var subcategories = [];
            var totalCount = 0;
            _.each(result.rows, function(subcategory) {
                var subcategoryDetails = {
                    "id": subcategory.id,
                    "name": subcategory.name,
                    "parent": subcategory.parent,
                    "createdOn": moment(subcategory.created_on).format('YYYY-MM-DD'),
                    "updated_on": subcategory.updated_on ? moment(subcategory.updated_on).format('YYYY-MM-DD') : null,
                    "status": subcategory.is_active
                };
                totalCount = parseInt(subcategory.full_count);
                subcategories.push(subcategoryDetails);
            });

            res.status(200).json({
                status: 'success',
                totalCount: totalCount,
                subcategories: subcategories
            });
        })
        .catch(function(err) {
            return next(err);
        });
}

async function getSubCategoryById(req, res, next) {
    const id = parseInt(req.params.id);
    var query = `SELECT sc.* from sub_category sc WHERE sc.id = $1`;

    pool.query(query, [id])
        .then(function(result) {
            var subcategoryDetails = {};
            _.each(result.rows, function(subcategory) {
                subcategoryDetails = {
                    "id": subcategory.id,
                    "name": subcategory.name,
                    "parent": subcategory.parent,
                    "createdOn": moment(subcategory.created_on).format('YYYY-MM-DD'),
                    "updated_on": subcategory.updated_on ? moment(subcategory.updated_on).format('YYYY-MM-DD') : null,
                    "status": subcategory.is_active
                };
            });

            res.status(200).json({
                status: 'success',
                subcategory: subcategoryDetails
            });
        })
        .catch(function(err) {
            return next(err);
        });

}

async function getSubCategoryByParentId(req, res, next) {
    const parentid = parseInt(req.params.parentid);
    var query = `SELECT sc.* from sub_category sc WHERE sc.parent = $1 and sc.is_active=$2`;

    pool.query(query, [parentid, true])
        .then(function(result) {
            var subcategories = [];
            _.each(result.rows, function(subcategory) {
                var subcategoryDetails = {
                    "id": subcategory.id,
                    "name": subcategory.name,
                    "parent": subcategory.parent,
                    "createdOn": moment(subcategory.created_on).format('YYYY-MM-DD'),
                    "updated_on": subcategory.updated_on ? moment(subcategory.updated_on).format('YYYY-MM-DD') : null,
                    "status": subcategory.is_active
                };
                subcategories.push(subcategoryDetails)
            });

            res.status(200).json({
                status: 'success',
                subcategory: subcategories
            });
        })
        .catch(function(err) {
            return next(err);
        });

}

async function removeSubCategory(req, res, next) {
    const id = parseInt(req.params.id);
    
    pool.query('select * from frame WHERE sub_category_id = $1', [id])
    .then(function(frameResult) {
        if(!_.isEmpty(frameResult.rows)){
            res.status(200).json({
                status: 'success',
                message: 'This SubCategory is assigned to frames. Please unassign it and try again'
            });
        } else {
            pool.query('UPDATE sub_category SET is_active = $2, updated_on = now() WHERE id = $1', [id, false])
            .then(function(result) {
                res.status(200).json({
                    status: 'success',
                    message: 'SubCategory has been deleted successfully!!!'
                });
            })
            .catch(function(err) {
                return next(err);
            });
        }        
    })
    .catch(function(err) {
        return next(err);
    });

    
}

async function reactivateSubCategory(req, res, next) {
    const id = parseInt(req.params.id);
    pool.query('UPDATE sub_category SET is_active = $2, updated_on = now() WHERE id = $1', [id, true])
    .then(function(result) {
        res.status(200).json({
            status: 'success',
            message: 'SubCategory has been reactivated successfully!!!'
        });
    })
    .catch(function(err) {
        return next(err);
    });
}



module.exports = {
    createSubCategory,
    updateSubCategory,
    getAllSubCategories,
    getSubCategoryById,
    getSubCategoryByParentId,
    removeSubCategory,
    reactivateSubCategory
};