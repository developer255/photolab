var pool = require('../../_helpers/config');
var _ = require('underscore');
var moment = require('moment');

async function createFrame(req, res, next) {
    var framedetails;
    if (typeof req.body.framedetails == 'string') {
        framedetails = JSON.parse(req.body.framedetails);
    } else {
        framedetails = req.body.framedetails
    }

    const { frameName, frameType, frameCode, frameDesc, categoryId, subCategoryId, qtyPrice } = framedetails;
    pool.query("INSERT INTO frame (frame_name, frame_type, frame_code, frame_desc, category_id ,sub_category_id, qty_price,created_on) VALUES ($1, $2, $3, $4, $5, $6, $7, now()) RETURNING id", [frameName, frameType, frameCode, frameDesc, categoryId, subCategoryId, qtyPrice])
        .then(function(result) {
            if (req.file) {
                uploadImage(result.rows[0].id, req.file, res)
            }

            res.status(200).json({
                status: 'success',
                message: 'Added frame successfully !!!'
            });
        })
        .catch(function(err) {
            return next(err);
        });
}

async function getFrames(req, res, next) {
    const { start, limit, status } = req.body
    var query = `SELECT f.*, i.img_name, i.img_path, c.id "category_id", c.name "category_name", sc.id "sub_category_id", sc.name "sub_category_name", count(f.*) OVER() AS full_count FROM frame f
    left join images i on i.entity_id = f.id and i.entity_type = $1 
    left join category c on c.id = f.category_id
    left join sub_category sc on sc.id = f.sub_category_id`;

    if (_.has(req.body, "status")) {
        query = `${query} where f.is_active = ${status}`
    }

    if (_.has(req.body, "start") && _.has(req.body, "limit")) {
        query = `${query} ORDER BY f.id ASC OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`
    }

    pool.query(query, ['frame'])
        .then(function(results) {
            var frames = [];
            var totalCount = 0;
            _.each(results.rows, function(frame) {
                var frameDetails = {
                    "id": frame.id,
                    "frameName": frame.frame_name,
                    "frameType": frame.frame_type,
                    "frameCode": frame.frame_code,
                    "frameDesc": frame.frame_desc,
                    "categoryId": frame.category_id,
                    "categoryName": frame.category_name,
                    "subCategoryId": frame.sub_category_id,
                    "subCategoryName": frame.sub_category_name,
                    "qtyPrice": frame.qty_price,
                    "status": frame.is_active,
                    "createdOn": frame.created_on ? moment(frame.created_on).format('YYYY-MM-DD') : null,
                    "updatedOn": frame.updated_on ? moment(frame.updated_on).format('YYYY-MM-DD') : null,
                    "imageName": frame.img_name,
                    "imagePath": frame.img_path,
                    "overallRating": 0
                };

                totalCount = frame.full_count;
                frames.push(frameDetails);
            });

            if (frames.length > 0) {
                respondFrames(res, { totalCount: totalCount, frames: frames });
            } else
                res.status(200).json({ totalCount: totalCount, frames: frames });
        })
        .catch(function(err) {
            return next(err);
        });
}

function respondFrames(response, frameObjects) {
    var pcount = 0;
    _.each(frameObjects.frames, function(frame) {
        pool.query(`select count(pr.*) reviewcount, SUM(pr.rating) sumofrating from frame_review pr where frame_id=$1`, [frame.id], function(err, res) {
            pcount = pcount + 1;
            if (!_.isEmpty(res.rows)) {
                var resObj = res.rows[0];
                var totalUserRated = parseInt(resObj.reviewcount);
                var sumOfMaxRatingOfReviewCount = (totalUserRated * 5);
                var sumOfRating = parseInt(resObj.sumofrating);
                if (!isNaN(sumOfRating)) {
                    frame.overallRating = (sumOfRating * 5) / sumOfMaxRatingOfReviewCount;
                }
            }

            if (pcount === frameObjects.frames.length) {
                response.status(200).json(frameObjects)
            }

        });
    });
}

async function getFrameById(req, res, next) {
    const id = parseInt(req.params.id);

    var query = `SELECT f.*, i.img_name, i.img_path, c.id "category_id", c.name "category_name", sc.id "sub_category_id", sc.name "sub_category_name" FROM frame f
    left join images i on i.entity_id = f.id and i.entity_type = $2 
    left join category c on c.id = f.category_id
    left join sub_category sc on sc.id = f.sub_category_id where f.id = $1`;

    pool.query(query, [id, 'frame'])
        .then(function(results) {
            var frameDetails = {};
            _.each(results.rows, function(frame) {
                frameDetails = {
                    "id": frame.id,
                    "frameName": frame.frame_name,
                    "frameType": frame.frame_type,
                    "frameCode": frame.frame_code,
                    "frameDesc": frame.frame_desc,
                    "categoryId": frame.category_id,
                    "categoryName": frame.category_name,
                    "subCategoryId": frame.sub_category_id,
                    "subCategoryName": frame.sub_category_name,
                    "qtyPrice": frame.qty_price,
                    "status": frame.is_active,
                    "createdOn": frame.created_on ? moment(frame.created_on).format('YYYY-MM-DD') : null,
                    "updatedOn": frame.updated_on ? moment(frame.updated_on).format('YYYY-MM-DD') : null,
                    "imageName": frame.img_name,
                    "imagePath": frame.img_path,
                    "overallRating": 0
                };

                pool.query(`select count(pr.*) reviewcount, SUM(pr.rating) sumofrating from frame_review pr where frame_id=$1`, [frame.id], function(err, review_res) {
                    if (!_.isEmpty(review_res.rows)) {

                        var resObj = review_res.rows[0];
                        var totalUserRated = parseInt(resObj.reviewcount);
                        var sumOfMaxRatingOfReviewCount = (totalUserRated * 5);
                        var sumOfRating = parseInt(resObj.sumofrating);
                        frameDetails.overallRating = (sumOfRating * 5) / sumOfMaxRatingOfReviewCount

                    }
                });
                getReviews(res, frameDetails);
            });

            // res.status(200).json(frameDetails);
        })
        .catch(function(err) {
            return next(err);
        });
}

function getReviews(response, frame) {
    var query = `SELECT r.id as "reviewId", r.review, c.full_name, r.rating from frame_review r
                        inner join customer c on c.id = r.customer_id
                        where r.frame_id = ${frame.id}`;

    pool.query(query, (error, results) => {
        if (error) {
            response.status(500).json({ "status": "Fail", "message": "Something went to wrong.Please contact to administrator." });
        }

        frame["reviews"] = []
        _.each(results.rows, function(reviewObj) {
            var reviewsDetails = {
                id: reviewObj.reviewId,
                review: reviewObj.review,
                userName: reviewObj.name,
                rating: reviewObj.rating
            }
            frame.reviews.push(reviewsDetails);
        });

        response.status(200).json(frame)
    });


}

async function removeFrame(req, res, next) {
    const id = parseInt(req.params.id);

    pool.query('update frame set is_active=$2, updated_on=now() where id=$1', [id, false])
        .then(function(result) {
            res.status(200).json({
                status: 'success',
                message: 'Frame deleted successfully !!!'
            });
        }).catch(function(err) {
            return next(err);
        });
}

async function reactivateFrame(req, res, next) {
    const id = parseInt(req.params.id);

    pool.query('update frame set is_active=$2,updated_on=now() where id=$1', [id, true])
        .then(function(result) {
            res.status(200).json({
                status: 'success',
                message: 'Frame reactivated successfully !!!'
            });
        }).catch(function(err) {
            return next(err);
        });
}

async function updateImage(req, res, next) {
    const id = parseInt(req.params.id);
    var file = req.file
    entityType = 'frame',
        entityId = id,
        imgName = file.filename,
        imgPath = '/frameImg/' + file.filename;

    pool.query("UPDATE images SET img_name = $1, img_path = $2 WHERE entity_id = $3 and entity_type = $4", [imgName, imgPath, entityId, entityType]).then(function(results) {
            res.status(200).json({ "status": "success", "message": "Frame image updated successfully." });
        })
        .catch(function(err) {
            return next(err);
        });
}

async function updateFrame(req, res, next) {
    const { id, frameName, frameType, frameCode, frameDesc, categoryId, subCategoryId, qtyPrice } = req.body;

    pool.query('UPDATE frame SET frame_name=$2, frame_type=$3, frame_code=$4, frame_desc=$5, category_id=$6 ,sub_category_id=$7, qty_price=$8, updated_on=now() WHERE id = $1', [id, frameName, frameType, frameCode, frameDesc, categoryId, subCategoryId, qtyPrice])
        .then(function(results) {
            res.status(200).json({ "status": "success", "message": "Frame details updated successfully." });
        })
        .catch(function(err) {
            return next(err);
        });
}

function uploadImage(frameId, file, res) {
    var entityType = 'frame',
        entityId = frameId,
        imgName = file.filename,
        imgPath = '/frameImg/' + file.filename;

    pool.query('INSERT INTO images (entity_type,entity_id,img_name, img_path,created_on) VALUES ($1, $2, $3, $4, now())', [entityType, entityId, imgName, imgPath], (error, result) => {
        if (error) {
            throw error
        }
        return true;
    })
}


async function createReview(req, res, next) {
    const { customerId, frameId, review, rating } = req.body;
    pool.query('INSERT INTO frame_review (customer_id, frame_id,review, rating, created_on) VALUES ($1, $2, $3, $4,now()) RETURNING id', [customerId, frameId, review, rating])
        .then(function(results) {
            res.status(200).json({ "status": "success", "message": "Product review updated successfully." });
        })
        .catch(function(err) {
            return next(err);
        });
}

async function updateReview(req, res, next) {
    const { reviewId, review, rating } = req.body;

    pool.query('update frame_review set review = $2, rating = $3, updated_on= now() where id=$1', [reviewId, review, rating])
        .then(function(results) {
            res.status(200).json({ "status": "success", "message": "Product review updated successfully." });
        })
        .catch(function(err) {
            return next(err);
        });
}


async function deleteReview(req, res, next) {
    var reviewId = parseInt(req.params.id);
    pool.query("delete from frame_review WHERE id = $1", [reviewId])
        .then(function(results) {
            res.status(200).json({ "status": "success", "message": "Product review deleted successfully." });
        })
        .catch(function(err) {
            return next(err);
        });
}

module.exports = {
    createFrame,
    getFrames,
    getFrameById,
    removeFrame,
    updateImage,
    updateFrame,
    reactivateFrame,
    createReview,
    updateReview,
    deleteReview
};