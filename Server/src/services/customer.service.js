var pool = require('../../_helpers/config');
var errorHandler = require('../../_helpers/error-handler')
var _ = require('underscore');
var moment = require('moment');
const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);

async function createCustomer(req, res, next) {    
    var userDetails, password = null, customerId = null;
    let randomString

    if(typeof req.body.userdetails == 'string'){
        userDetails = JSON.parse(req.body.userdetails);
    } else {
        userDetails = req.body.userdetails
    }
    const {name, email, mobile, user_name} = userDetails;
    const roleId = userDetails.roleId; 
    if(userDetails.password){
        password = encrypt(userDetails.password);
    } else {
        randomString = randomStringGen()
    }
        
    pool.query(`select * from customer where user_name='${user_name}' or email='${email}'`).then(function (userExistResult) {
        if(userExistResult && userExistResult.rows.length>0){
            var userObj = userExistResult.rows[0];
            if(userObj.is_active === false){
                res.status(500).json({"status": "Fail", "message":"Your account is deactivated. Please contact to administrator for activate your account."});
            } else {
                res.status(500).json({"status": "success", "message":"Mobile number or email is already registerd in system.Please contact to administrator."});
            }                
        } else {
            var query = "INSERT INTO customer (full_name, user_name, email, customer_refer_code, mobile_no,created_on) VALUES ($1, $2, $3, $4, $5,now()) RETURNING id",
                queryParams = [name, user_name, email, randomString, mobile];

            if(userDetails.password && password){
                query = "INSERT INTO customer (full_name, user_name, password, email, iv, envkey, mobile_no, created_on) VALUES ($1, $2, $3, $4, $5, $6, $7,now()) RETURNING id"
                queryParams = [name, user_name, password.encryptedData ,email, password.iv, password.key, mobile]
            } 

            pool.query(query, queryParams).then(function (result) {
                customerId = result.rows[0].id;
                if(customerId){
                  assignRole(roleId, customerId);
                  if(req.file){
                      uploadImage(customerId, req.file, res)
                  }
                }      
                res.status(200).json({"status": "success", "message":"User has been created successfully."});
            })
            .catch(function (err) {
                return next(err);
            });
        }
    })
    .catch(function (err) {
        return next(err);
    });
}

async function updateCustomer(req, res, next) {
    const { name,email,dob, gender, id} = req.body

    pool.query('UPDATE customer SET full_name = $1, email = $2, date_of_birth = $3, gender = $4 WHERE id = $5',
    [name,email,dob, gender, id])
    .then(function (result) {
        res.status(200).json({"status": "success", "message":"User details has been updated successfully."});
    })
    .catch(function (err) {
        return next(err);
    });
}

async function reactivateCustomer(req, res, next) {
    const id = parseInt(req.params.id)

    pool.query('UPDATE customer SET is_active = $1 WHERE id = $2',[true, id])
    .then(function (result) {
        res.status(200).json({"status": "success", "message":"User has been reactivated successfully."});
    })
    .catch(function (err) {
        return next(err);
    });
}

async function deleteCustomer(req, res, next) {
    const id = parseInt(req.params.id)

    pool.query('UPDATE customer SET is_active = $1 WHERE id = $2', [false, id])
    .then(function (result) {
        res.status(200).json({"status": "success", "message":"User has been deactivated successfully."});
    })
    .catch(function (err) {
        return next(err);
    });
}

async function getCustomers(req, res, next) {
    const { start, limit} = req.body
    var entityType = 'customer';

    var query = `SELECT c.id, c.full_name, c.user_name, c.email,c.mobile_no, c.date_of_birth, c.gender, c.is_active,i.img_name, i.img_path, count(c.*) OVER() AS full_count FROM customer c
                left join images i on i.entity_id = c.id and i.entity_type = $1 ORDER BY id ASC`;

    if(_.has(req.body, "start") && _.has(req.body, "limit")){
        query = `${query} OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`
    }

    pool.query(query, [entityType])
    .then(function (results) {
        var users = [];
        var totalCount = 0;
        _.each(results.rows, function(user){
            var userDetails = {
                "id": user.id,
                "name": user.full_name,
                "userName": user.user_name,
                "email": user.email,
                "date_of_birth": user.date_of_birth ? moment(user.date_of_birth).format('YYYY-MM-DD') : null,
                "mobile_no": user.mobile_no,
                "gender": user.gender,
                "active": user.is_active,
                "imageName": user.img_name,
                "imagePath": user.img_path
            };

            totalCount = user.full_count;
            users.push(userDetails);
        });

        res.status(200).json({totalCount: totalCount ,users: users});
    })
    .catch(function (err) {
        return next(err);
    });
}

async function getCustomerById(req, res, next) {
    const id = parseInt(req.params.id);

    var entityType = 'customer';
    var query = `SELECT c.id, c.full_name, c.user_name, c.email,c.mobile_no, c.date_of_birth, c.gender, c.is_active,i.img_name, i.img_path FROM customer c
    left join images i on i.entity_id = c.id and i.entity_type = $2 WHERE c.id = $1`;

    pool.query(query, [id,entityType])
    .then(function (results) {
        var userDetails = {};
        _.each(results.rows, function(user){
            userDetails = {
                "id": user.id,
                "name": user.full_name,
                "userName": user.user_name,
                "email": user.email,
                "date_of_birth": user.date_of_birth ? moment(user.date_of_birth).format('YYYY-MM-DD') : null,
                "mobile_no": user.mobile_no,
                "gender": user.gender,
                "active": user.is_active,
                "imageName": user.img_name,
                "imagePath": user.img_path
            };
        });

        res.status(200).json(userDetails);
    })
    .catch(function (err) {
        return next(err);
    });
}

async function updateUserImage(req, res, next) {
    const id = parseInt(req.params.id);
    var file = req.file
    entityType = 'customer',
    entityId = id,
    imgName = file.filename,
    imgPath = '/customerImg/' + file.filename;

    pool.query("UPDATE images SET img_name = $1, img_path = $2 WHERE entity_id = $3 and entity_type = $4",
    [imgName, imgPath, entityId, entityType]).then(function (results) {
        res.status(200).json({"status": "success", "message":"Customer profile image updated successfully."});
    })
    .catch(function (err) {
        return next(err);
    });
}

async function addAddress(req, res, next) {  
    const {customerId, address, city, state, country, pincode, landMark, addressType, mobile_no} = req.body;
    
    pool.query('INSERT INTO customer_address (customer_id, address, city, state, country, pincode, land_mark, address_type, is_default, mobile_no, created_on) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, now()) RETURNING id', 
    [customerId, address, city, state, country, pincode, landMark ,addressType, false, mobile_no]).then(function (result) {
        var newAddressId = result.rows[0].id;

        pool.query("select * from customer_address WHERE customer_id = $1",[customerId])
        .then(function (addressResult) {
            if(addressResult.rows.length === 1){

                pool.query("UPDATE customer_address SET is_default = true WHERE id = $1 and customer_id = $2",
                    [newAddressId, customerId])
                .then(function (setDefaultResult) {
                    res.status(200).json({"status": "success", "message":"Customer address added successfully."});
                })
                .catch(function (err) {
                    return next(err);
                });
    
            } else {
                res.status(200).json({"status": "success", "message":"Customer address added successfully."});
            }
        })
        .catch(function (err) {
            return next(err);
        });

        
    })
    .catch(function (err) {
        return next(err);
    });
}
  
async function getAddress(req, res, next) {
    const id = parseInt(req.params.id)
    var query = `SELECT id, address, city, state, country, pincode, land_mark, address_type, is_active, is_default, mobile_no from customer_address WHERE customer_id = $1 and is_active = $2`;
  
    pool.query(query , [id, true]).then(function (results) {
        if(_.isEmpty(results.rows)){
            res.status(200).json([])
        } else {
            res.status(200).json(results.rows)
        }
    })
    .catch(function (err) {
        return next(err);
    });
}

async function updateAddress(req, res, next) { 
    const {id, address, city, state, country, pincode, landMark, addressType, mobile_no} = req.body; 
    
    pool.query('UPDATE customer_address SET address=$2, city=$3, state=$4, country=$5, pincode=$6, address_type=$7, mobile_no=$8, land_mark=$9,updated_on=now() WHERE id = $1', [id, address, city, state, country, pincode, addressType, mobile_no, landMark])
    .then(function (results) {
        res.status(200).json({"status": "success", "message":"Customer address details updated successfully."});
    })
    .catch(function (err) {
        return next(err);
    });
}

async function removeAddress(req, res, next) { 
    const id = parseInt(req.params.id); 

    pool.query('UPDATE customer_address SET is_active = $1, is_default= $2 WHERE id = $3 and is_default = $4', [false, false , id, false])
    .then(function (results) {
        if(_.isEmpty(results.rows) && results.rowCount ===0){
            res.status(500).json({"status": "fail", "message":"Please change default address then try again."});    
        } else {
            res.status(200).json({"status": "success", "message":"Address has been deleted successfully."});
        }
    })
    .catch(function (err) {
        return next(err);
    });
}

async function changeDefaultAddress(req, res, next) { 
    const {id, customerId} = req.body; 

    pool.query('UPDATE customer_address SET is_default=$2, updated_on=now() WHERE id = $1', [id, true])
    .then(function (results) {
        pool.query("UPDATE customer_address SET is_default = false, updated_on=now() WHERE id <> $1 and customer_id=$2",
        [id, customerId])
        .then(function (defaultSetResults) {
            res.status(200).json({"status": "success", "message":"Changed customer default address successfully."});
        })
        .catch(function (err) {
            return next(err);
        });
    })
    .catch(function (err) {
        return next(err);
    });
}


function assignRole(roleId, customerId){
    pool.query('INSERT INTO customer_role (customer_id,role_id, created_on) VALUES ($1, $2, now())', [customerId,roleId], (error, result) => {
        if (error) {
          //throw error
          return false;
        }
        return true;
    })
}

function uploadImage(customerId, file, res){
    var entityType = 'customer',
        entityId = customerId,
        imgName = file.filename,
        imgPath = '/customerImg/' + file.filename;

    pool.query('INSERT INTO images (entity_type,entity_id,img_name, img_path,created_on) VALUES ($1, $2, $3, $4, now())', [entityType,entityId, imgName, imgPath], (error, result) => {
        if (error) {
            throw error
        }
        return true;
    })
}

function randomStringGen() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for (var i=0; i<string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum,rnum+1);
    }

    return randomstring.toString();
    
}

function encrypt(text) {
    let cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex'), key : key.toString('hex') };
}

module.exports = {
    createCustomer,
    updateCustomer,
    deleteCustomer,
    reactivateCustomer,
    getCustomers,
    getCustomerById,
    updateUserImage,
    addAddress,
    getAddress,
    updateAddress,
    removeAddress,
    changeDefaultAddress
};