var pool = require('../../_helpers/config');
var _ = require('underscore');
var moment = require('moment');

async function createOrderStatus(req, res, next) {
    const {name} = req.body    
    pool.query('INSERT INTO order_status (status_title, created_on) VALUES ($1,now()) RETURNING id', [name])
        .then(function (result) {
            res.status(200).json({ 
                status: 'success', 
                message: 'Order status created successfully !!!'
            });
        })
        .catch(function (err) {
            return next(err);
        });
}

async function getAllOrderStatus(req, res, next) {    
    const {status} = req.body    
    var query = `SELECT os.* from order_status os`;
    
    if(_.has(req.body, "status")){
        query = `${query} where os.is_active = ${status} ORDER BY os.id ASC`;  
    }

    pool.query(query)
    .then(function (result) {        
        var orderStatus = [];
        _.each(result.rows, function(status){
            var orderStatusDetails = {
                "id": status.id,
                "name": status.status_title,
                "status": status.is_active
            };
            orderStatus.push(orderStatusDetails);
        });

        res.status(200).json({
            status: 'success',
            orderStatus:orderStatus
        });
    })
    .catch(function (err) {
        return next(err);
    });
}

async function getOrderStatusById(req, res, next) {   
    const id = parseInt(req.params.id);
    var query = `SELECT os.* from order_status os WHERE os.id = $1`;

    pool.query(query, [id])
    .then(function (result) {        
        var orderStatusDetails = {};
        _.each(result.rows, function(status){
            orderStatusDetails = {
                "id": status.id,
                "name": status.status_title,
                "status": status.is_active
            };
        });

        res.status(200).json(orderStatusDetails);
    })
    .catch(function (err) {
        return next(err);
    });
}

async function updateOrderStatus(req, res, next) {
    const {id,name} = req.body
    pool.query('UPDATE order_status SET status_title = $2, updated_on = now() WHERE id = $1', [id, name])
        .then(function (result) {
            res.status(200).json({
                status: 'success', 
                message: 'Order Status has been updated successfully!!!'
            });
        })
        .catch(function (err) {
            return next(err);
        });
}

async function removeOrderStatus(req, res, next) {
    const id = parseInt(req.params.id);

    pool.query('UPDATE order_status SET is_active = false, updated_on = now() WHERE id = $1', [id])
    .then(function (result) {
        res.status(200).json({
            status: 'success', 
            message: 'Order Status has been deleted successfully!!!'
        });
    })
    .catch(function (err) {
        return errorHandler(err, req, res, next);
    });

    
}

async function reactivateOrderStatus(req, res, next) {
    const id = parseInt(req.params.id);
    pool.query(`UPDATE order_status SET is_active = true, updated_on = now() WHERE id = $1`, [id])
        .then(function (result) {
            res.status(200).json({
                status: 'success', 
                message: 'Order Status has been reactivated successfully!!!'
            });
        })
        .catch(function (err) {
            return next(err);
        });
}


module.exports = {
    createOrderStatus,
    getAllOrderStatus,
    getOrderStatusById,
    updateOrderStatus,
    removeOrderStatus,
    reactivateOrderStatus
};