var pool = require('../../_helpers/config');
var _ = require('underscore');
var moment = require('moment');

async function createShippingCharges(req, res, next) {
    const {amountSlab, charges} = req.body;
    pool.query('INSERT INTO shipping_charges (amount_slab, charges, created_on) VALUES ($1, $2,now()) RETURNING id', [amountSlab, charges])
        .then(function (result) {
            res.status(200).json({ 
                status: 'success', 
                message: 'Shipping charges created successfully !!!'
            });
        })
        .catch(function (err) {
            return next(err);
        });
}

async function getAllShippingCharges(req, res, next) {    
    const {status} = req.body    
    var query = `SELECT sc.* from shipping_charges sc`;
    
    if(_.has(req.body, "status")){
        query = `${query} where sc.is_active = ${status} ORDER BY sc.id ASC`;  
    }

    pool.query(query)
    .then(function (result) {        
        var shippingCharges = [];
        _.each(result.rows, function(charges){
            var shippingChargesDetails = {
                "id": charges.id,
                "slab": charges.amount_slab,
                "charges": charges.charges,
                "status": charges.is_active
            };
            shippingCharges.push(shippingChargesDetails);
        });

        res.status(200).json({
            status: 'success',
            shippingCharges:shippingCharges
        });
    })
    .catch(function (err) {
        return next(err);
    });
}

async function getShippingChargesById(req, res, next) {   
    const id = parseInt(req.params.id);
    var query = `SELECT sc.* from shipping_charges sc WHERE sc.id = $1`;

    pool.query(query, [id])
    .then(function (result) {        
        var shippingChargesDetails = {};
        _.each(result.rows, function(shippingCharges){
            shippingChargesDetails = {
                "id": shippingCharges.id,
                "slab": shippingCharges.amount_slab,
                "charges": shippingCharges.charges,
                "status": shippingCharges.is_active
            };
        });

        res.status(200).json(shippingChargesDetails);
    })
    .catch(function (err) {
        return next(err);
    });
}

async function updateShippingCharges(req, res, next) {
    const {id, amountSlab, charges} = req.body    
    pool.query('UPDATE shipping_charges SET amount_slab = $2, charges=$3, updated_on = now() WHERE id = $1', [id,  amountSlab, charges])
        .then(function (result) {
            res.status(200).json({
                status: 'success', 
                message: 'Shipping charges has been updated successfully!!!'
            });
        })
        .catch(function (err) {
            return next(err);
        });
}

async function removeShippingCharges(req, res, next) {
    const id = parseInt(req.params.id);

    pool.query('UPDATE shipping_charges SET is_active = false, updated_on = now() WHERE id = $1', [id])
    .then(function (result) {
        res.status(200).json({
            status: 'success', 
            message: 'Shipping charges has been deleted successfully!!!'
        });
    })
    .catch(function (err) {
        return errorHandler(err, req, res, next);
    });

    
}

async function reactivateShippingCharges(req, res, next) {
    const id = parseInt(req.params.id);
    pool.query(`UPDATE shipping_charges SET is_active = true, updated_on = now() WHERE id = $1`, [id])
        .then(function (result) {
            res.status(200).json({
                status: 'success', 
                message: 'Shipping charges has been reactivated successfully!!!'
            });
        })
        .catch(function (err) {
            return next(err);
        });
}


module.exports = {
    createShippingCharges,
    getAllShippingCharges,
    getShippingChargesById,
    updateShippingCharges,
    removeShippingCharges,
    reactivateShippingCharges
};