var pool = require('../../_helpers/config');
var errorHandler = require('../../_helpers/error-handler')
var _ = require('underscore');
var moment = require('moment');


// function to create category
async function createCategory(req, res, next) {
    const { name } = req.body
    pool.query('INSERT INTO category (name, created_on) VALUES ($1,now()) RETURNING id', [name])
        .then(function(result) {
            const { category } = result.rows[0].id;
            res.status(200).json({
                status: 'success',
                message: 'Inserted one category with id ' + result.rows[0].id
            });
        })
        .catch(function(err) {
            // Note : if an exception is thrown then this will redirect to error-handler.js file
            // where we can handle the exception logging 
            // please go to error-hanler.js file to see my comments
            // please remove this comments once u get the idea
            return next(err);
        });
}

// function to update category
async function updateCategory(req, res, next) {
    const { id, name } = req.body
    pool.query('UPDATE category SET name = $2, updated_on = now() WHERE id = $1', [id, name])
        .then(function(result) {
            res.status(200).json({
                status: 'success',
                message: 'Category has been updated successfully!!!'
            });
        })
        .catch(function(err) {
            return next(err);
        });
}

// function to get category list
async function getAllCategories(req, res, next) {
    const { start, limit, status } = req.body
    var query = `SELECT c.*, count(c.*) OVER() AS full_count from category c ORDER BY id ASC`;
    if (_.has(req.body, "start") && _.has(req.body, "limit")) {
        query = `${query} OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`
    }

    if (_.has(req.body, "status")) {
        query = `SELECT c.*, count(c.*) OVER() AS full_count from category c where c.is_active = ${status} ORDER BY id ASC`;
        if (_.has(req.body, "start") && _.has(req.body, "limit")) {
            query = `${query} OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`
        }
    }

    pool.query(query)
        .then(function(result) {
            var categories = [];
            var totalCount = 0;
            _.each(result.rows, function(category) {
                var categoryDetails = {
                    "id": category.id,
                    "name": category.name,
                    "createdOn": moment(category.created_on).format('YYYY-MM-DD'),
                    "updated_on": category.updated_on ? moment(category.updated_on).format('YYYY-MM-DD') : null,
                    "status": category.is_active
                };
                totalCount = parseInt(category.full_count);
                categories.push(categoryDetails);
            });

            res.status(200).json({
                status: 'success',
                totalCount: totalCount,
                categories: categories
            });
        })
        .catch(function(err) {
            return next(err);
        });
}

async function getCategoryById(req, res, next) {
    const id = parseInt(req.params.id);
    var query = `SELECT c.* from category c WHERE c.id = $1`;

    pool.query(query, [id])
        .then(function(result) {
            var categoryDetails = {};
            _.each(result.rows, function(category) {
                categoryDetails = {
                    "id": category.id,
                    "name": category.name,
                    "createdOn": moment(category.created_on).format('YYYY-MM-DD'),
                    "updated_on": category.updated_on ? moment(category.updated_on).format('YYYY-MM-DD') : null,
                    "status": category.is_active
                };
            });

            res.status(200).json({
                status: 'success',
                category: categoryDetails
            });
        })
        .catch(function(err) {
            return next(err);
        });

}

async function removeCategory(req, res, next) {
    const id = parseInt(req.params.id);
    
    pool.query('select * from sub_category WHERE parent = $1 and is_active=$2', [id, true])
    .then(function(subCategoryResult) {
        if(!_.isEmpty(subCategoryResult.rows)){
            res.status(200).json({
                status: 'success',
                message: 'This catgeory has some subcategories. Please unassign it or remove it and try again'
            });
        } else {
            pool.query('UPDATE category SET is_active = $2, updated_on = now() WHERE id = $1', [id, false])
            .then(function(result) {
                res.status(200).json({
                    status: 'success',
                    message: 'Category has been deleted successfully!!!'
                });
            })
            .catch(function(err) {
                return next(err);
            });  
        }
          
    })
    .catch(function(err) {
        return next(err);
    });

    
}

async function reactivateCategory(req, res, next) {
    const id = parseInt(req.params.id);
    pool.query('UPDATE category SET is_active = $2, updated_on = now() WHERE id = $1', [id, true])
        .then(function(result) {
            res.status(200).json({
                status: 'success',
                message: 'Category has been reactivated successfully!!!'
            });
        })
        .catch(function(err) {
            return next(err);
        });
}



module.exports = {
    createCategory,
    updateCategory,
    getAllCategories,
    getCategoryById,
    removeCategory,
    reactivateCategory
};