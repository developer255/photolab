var pool = require('../../_helpers/config');
var _ = require('underscore');
var moment = require('moment');

async function createOffer(req, res, next) {
    var offerdetails;
    if (typeof req.body.offerdetails == 'string') {
        console.log("in if");
        offerdetails = JSON.parse(req.body.offerdetails);
    } else {
        console.log("in else");
        offerdetails = req.body.offerdetails
    }

    console.log("in if", offerdetails);
    const { offerValue, offerType, offerDesc, offerCode, validFrom, validTo, point } = offerdetails;
    pool.query(`select * from offer where offer_code='${offerCode}'`)
        .then(function(result) {
            if (result && result.rows.length > 0) {
                res.status(500).json({ "status": "Success", "message": "Offer code is already registerd in system.Please try again with new code." });
            } else {
                console.log("insert");
                pool.query('INSERT INTO offer (offer_value,offer_type,offer_desc,offer_code, from_date,to_date, point,created_on) VALUES ($1, $2, $3, $4, $5,$6,$7 ,now()) RETURNING id', [offerValue, offerType, offerDesc, offerCode, validFrom, validTo, point])
                    .then(function(offerResult) {
                        if (req.file) {
                            uploadImage(offerResult.rows[0].id, req.file, res)
                        }

                        res.status(200).json({ "status": "Success", "message": "Offer created successfully." });
                    })
                    .catch(function(err) {
                        return next(err);
                    });
            }
        })
        .catch(function(err) {
            return next(err);
        });
}

async function getAllOffers(req, res, next) {
    const { start, limit } = req.body
    var query = `SELECT o.id offer_id, o.offer_value, o.offer_type,o.offer_desc, o.offer_code, o.from_date, o.to_date, o.point, i.img_name, i.img_path, count(o.*) OVER() AS full_count  FROM offer o
                 left join images i on i.entity_id = o.id and i.entity_type = $1 where o.is_active = true
                 ORDER BY o.id ASC`;

    if (_.has(req.body, "start") && _.has(req.body, "limit")) {
        query = `${query} OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`;
    }

    pool.query(query, ['offer'])
        .then(function(results) {
            var offers = [];
            var totalCount = 0;
            _.each(results.rows, function(offer) {
                var offerDetails = {
                    o_id: offer.offer_id,
                    offerValue: offer.offer_value,
                    offerType: offer.offer_type,
                    offerDesc: offer.offer_desc,
                    imageName: offer.img_name,
                    imagePath: offer.img_path,
                    offerCode: offer.offer_code,
                    point: offer.point,
                    offerValidFrom: moment(offer.from_date).format('YYYY-MM-DD'),
                    offerValidTo: moment(offer.to_date).format('YYYY-MM-DD')
                }
                totalCount = offer.full_count;
                offers.push(offerDetails);
            });
            res.status(200).json({ totalCount: totalCount, offers: offers });
        })
        .catch(function(err) {
            return next(err);
        });
}

async function getOfferById(req, res, next) {
    const id = parseInt(req.params.id);

    var query = `SELECT o.id "offer_id", o.offer_value, o.offer_type,o.offer_desc,o.offer_code, o.from_date, o.to_date, o.point, i.img_name, i.img_path  FROM offer o
                left join images i on i.entity_id = o.id and i.entity_type = $2
                WHERE o.id = $1
                ORDER BY o.id ASC`

    console.log(query);
    pool.query(query, [id, 'offer'])
        .then(function(results) {
            var offerDetails = {};
            _.each(results.rows, function(offer) {
                offerDetails = {
                    o_id: offer.id,
                    offerValue: offer.offer_value,
                    offerType: offer.offer_type,
                    offerDesc: offer.offer_desc,
                    imageName: offer.img_name,
                    imagePath: offer.img_path,
                    offerCode: offer.offer_code,
                    point: offer.point,
                    offerValidFrom: moment(offer.from_date).format('YYYY-MM-DD'),
                    offerValidTo: moment(offer.to_date).format('YYYY-MM-DD')
                }
            });

            res.status(200).json(offerDetails);
        })
        .catch(function(err) {
            return next(err);
        });
}

async function removeOffer(req, res, next) {
    const id = parseInt(req.params.id);

    var checkOfferInOrder = `select count(o.*) orderactivecount from orders o
                               where o.offer_id = ${id}`
    var today = moment(new Date()).format('YYYY-MM-DD');

    pool.query(checkOfferInOrder)
        .then(function(checkResult) {
            if (parseInt(checkResult.rows[0].orderactivecount) > 0) {
                res.status(500).json({ "status": "fail", "message": "This offer is applied on some orders. Please change the status of order and try again" });
            } else {
                pool.query("UPDATE offer SET is_active=false , to_date=$2, updated_on=now() WHERE id = $1", [id, today])
                    .then(function(result) {
                        res.status(200).json({ "status": "success", "message": "Offer deleted successfully." });
                    }).catch(function(err) {
                        return next(err);
                    });
            }
        }).catch(function(err) {
            return next(err);
        });


}

async function validateOfferCode(req, res, next) {
    const { offerCode } = req.body

    var query = `select * from offer where offer_code = '${offerCode}'`
    pool.query(query)
        .then(function(results) {
            if (_.isEmpty(results.rows)) {
                res.status(200).json({ "status": "Success", "message": "Invalid offer code" });
            } else {
                var offerObj = results.rows[0];
                var offerDetails = {
                    o_id: offerObj.id,
                    offerValue: offerObj.offer_value,
                    offerType: offerObj.offer_type,
                    offerDesc: offerObj.offer_desc,
                    offerCode: offerObj.offer_code,
                    point: offerObj.point,
                    offerValidFrom: moment(offerObj.from_date).format('YYYY-MM-DD'),
                    offerValidTo: moment(offerObj.to_date).format('YYYY-MM-DD')
                }

                if (offerObj.is_active) {
                    res.status(200).json(offerDetails);
                } else {
                    res.status(200).json({ "status": "Success", "message": "Offer expired." });
                }
            }
        }).catch(function(err) {
            return next(err);
        });


}

async function updateImage(req, res, next) {
    const id = parseInt(req.params.id);
    var file = req.file
    entityType = 'offer',
        entityId = id,
        imgName = file.filename,
        imgPath = '/offerImg/' + file.filename;

    pool.query("UPDATE images SET img_name = $1, img_path = $2 WHERE entity_id = $3 and entity_type = $4", [imgName, imgPath, entityId, entityType]).then(function(results) {
            res.status(200).json({ "status": "success", "message": "Offer image updated successfully." });
        })
        .catch(function(err) {
            return next(err);
        });
}

async function updateOffer(req, res, next) {
    const { offerValue, offerType, offerDesc, id, offerCode, validFrom, validTo, point } = req.body;

    pool.query("UPDATE offer SET offer_value = $1,offer_type =$2 ,offer_desc = $3, offer_code=$5, from_date=$6, to_date=$7, point=$8  WHERE id = $4", [offerValue, offerType, offerDesc, id, offerCode, validFrom, validTo, point])
        .then(function(results) {
            res.status(200).json({ "status": "success", "message": "Offer details has been updated successfully." });
        })
        .catch(function(err) {
            return next(err);
        });
}

function uploadImage(offerId, file, res) {
    var entityType = 'offer',
        entityId = offerId,
        imgName = file.filename,
        imgPath = '/offerImg/' + file.filename;

    pool.query('INSERT INTO images (entity_type,entity_id,img_name, img_path,created_on) VALUES ($1, $2, $3, $4, now())', [entityType, entityId, imgName, imgPath], (error, result) => {
        if (error) {
            throw error
        }
        return true;
    })
}


module.exports = {
    createOffer,
    getAllOffers,
    getOfferById,
    removeOffer,
    updateImage,
    updateOffer,
    validateOfferCode
};