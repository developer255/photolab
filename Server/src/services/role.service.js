var pool = require('../../_helpers/config');
var errorHandler = require('../../_helpers/error-handler')
var _ = require('underscore');
var moment = require('moment');

async function createRole(req, res, next) {
    const {name} = req.body    
    pool.query('INSERT INTO role (role_name, created_on) VALUES ($1,now()) RETURNING id', [name])
        .then(function (result) {
            const { role } = result.rows[0].id;
            res.status(200).json({ 
                status: 'success', 
                message: 'Inserted one role with id ' + result.rows[0].id
            });
        })
        .catch(function (err) {
            return next(err);
        });
}

async function getAllRoles(req, res, next) {    
    const { start, limit, status} = req.body    
    var query = `SELECT r.*, count(r.*) OVER() AS full_count from role r ORDER BY id ASC`;
    if(_.has(req.body, "start") && _.has(req.body, "limit")){
        query = `${query} OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`
    }

    if(_.has(req.body, "status")){
        query = `SELECT r.*, count(r.*) OVER() AS full_count from role r where r.is_active = ${status} ORDER BY id ASC`;
        if(_.has(req.body, "start") && _.has(req.body, "limit")){
            query = `${query} OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`
        }  
    }

    pool.query(query)
    .then(function (result) {        
        var roles = [];
        var totalCount = 0;
        _.each(result.rows, function(role){
            var roleDetails = {
                "id": role.id,
                "name": role.role_name,
                "createdOn": moment(role.created_on).format('YYYY-MM-DD'),
                "updated_on" : role.updated_on ? moment(role.updated_on).format('YYYY-MM-DD') : null,
                "status": role.is_active
            };
            totalCount = parseInt(role.full_count);
            roles.push(roleDetails);
        });

        res.status(200).json({
            status: 'success',
            totalCount: totalCount,
            roles: roles
        });
    })
    .catch(function (err) {
        return next(err);
    });
}

async function getRoleById(req, res, next) {    
    const id = parseInt(req.params.id);
    var query = `SELECT r.* from role r WHERE r.id = $1`;

    pool.query(query, [id])
    .then(function (result) {        
        var roleDetails = {};
        _.each(result.rows, function(role){
            roleDetails = {
                "id": role.id,
                "name": role.role_name,
                "createdOn": moment(role.created_on).format('YYYY-MM-DD'),
                "updated_on" : role.updated_on ? moment(role.updated_on).format('YYYY-MM-DD') : null,
                "status": role.is_active
            };
        });

        res.status(200).json({
            status: 'success',
            role: roleDetails
        });
    })
    .catch(function (err) {
        return next(err);
    });
}

async function updateRole(req, res, next) {
    const {id,name} = req.body    
    pool.query('UPDATE role SET role_name = $2, updated_on = now() WHERE id = $1', [id, name])
        .then(function (result) {
            res.status(200).json({
                status: 'success', 
                message: 'Role has been updated successfully!!!'
            });
        })
        .catch(function (err) {
            return next(err);
        });
}

async function removeRole(req, res, next) {
    const id = parseInt(req.params.id);

    pool.query('select * from customer_role where role_id=$1', [id])
    .then(function (result) {
        if(result.rows.length == 0){
            pool.query('UPDATE role SET is_active = false, updated_on = now() WHERE id = $1', [id])
            .then(function (result) {
                res.status(200).json({
                    status: 'success', 
                    message: 'Role has been deleted successfully!!!'
                });
            })
            .catch(function (err) {
                return errorHandler(err, req, res, next);
            });
        } else {
            res.status(200).json({
                status: 'success', 
                message: 'This Role has assigned customers, Please try later.'
            });
        }
        
    }).catch(function (err) {
        return next(err);
    });

    
}

async function reactivateRole(req, res, next) {
    const id = parseInt(req.params.id);
    pool.query(`UPDATE role SET is_active = true, updated_on = now() WHERE id = $1`, [id])
        .then(function (result) {
            res.status(200).json({
                status: 'success', 
                message: 'Role has been reactivated successfully!!!'
            });
        })
        .catch(function (err) {
            return next(err);
        });
}


module.exports = {
    createRole,
    getAllRoles,
    getRoleById,
    updateRole,
    removeRole,
    reactivateRole
};