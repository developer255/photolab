var pool = require('../../_helpers/config');
var _ = require('underscore');
const rp = require('request-promise-native');
var moment = require('moment');

async function createOrder(req, res, next) {
    var orderDetails ;
    if(typeof req.body.orderDetails == 'string'){
        orderDetails = JSON.parse(req.body.orderDetails);
    } else {
        orderDetails = req.body.orderDetails
    }
    const {customerId, totalAmount, orderDate, offerId, shippingChargesId, orderStatusId} = orderDetails
    var frameList = JSON.stringify(orderDetails.frameList);
    var paymentDetails = orderDetails.paymentDetails;
    var orderId = null;

    pool.query('INSERT INTO orders (customer_id, total_amount, order_date , offer_id, shipping_charges_id, order_status_id, created_on) VALUES ($1, $2, $3, $4, $5, $6,now())  RETURNING id', [customerId, totalAmount, orderDate, offerId, shippingChargesId, orderStatusId])
    .then(function (result) {
        orderId = result.rows[0].id;
        initOrderItems(orderId, frameList)
        if(!_.isEmpty(paymentDetails)){               
            updatePayment(orderId, paymentDetails, next);
        } 
    })
    .catch(function (err) {
        return next(err);
    });

    function initOrderItems(orderId, frameList){
        var frameList = JSON.parse(frameList);

        _.each(frameList, function(frame, frameIndex) {
            
            var frameId = frame.frameId,
                frameName = frame.frameName,
                quantity = frame.quantity,
                price = frame.price,
                nameOnFrame = frame.nameonframe
                deliveryDate = frame.deliveryDate,
                occasion = frame.occasion;

                pool.query('INSERT INTO order_items (order_id ,frame_id, quantity, price, name_on_frame, delivery_date, occasion) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id', [orderId ,frameId, quantity, price, nameOnFrame,deliveryDate, occasion])
                .then(function (result) {
                    if(req.files.length > 0){
                        uploadImage(result.rows[0].id, req.files[frameName][0], res)
                    }                   
                    
                    if(frameList.length === frameIndex + 1){
                        res.status(200).json({ 
                            status: 'success', 
                            message: 'Order has been placed successfully !!!'
                        });
                    }
                })
                .catch(function (err) {
                    return next(err);
                });
        });
    }
}

async function orderPayment(req, res, next) {  
    const { orderId } = req.body
    var paymentDetails = req.body.paymentDetails;
    await updatePayment(orderId, paymentDetails, next, true, res);
}

function updatePayment(orderId, paymentDetails, next, isApiCall = false, res = null){
    const { paymentMode, paymentStatus, transactionNum, paymentDate} = paymentDetails;
    
    pool.query('update orders SET payment_mode=$2, payment_status= $3, transaction_num= $4, payment_date= $5, updated_on=now() WHERE id = $1', [orderId, paymentMode, paymentStatus, transactionNum, paymentDate])
    .then(function (result) {
        if(isApiCall){
            res.status(200).json({ 
                status: 'success', 
                message: 'Payment has been done successfully !!!'
            });
        } else {
            return true;
        }
        
    })
    .catch(function (err) {
        return next(err);
    });
}

function uploadImage(orderItemId, file, res){
    var entityType = 'order',
        entityId = orderItemId,
        imgName = file.filename,
        imgPath = '/orderImg/' + file.filename;

    pool.query('INSERT INTO images (entity_type,entity_id,img_name, img_path,created_on) VALUES ($1, $2, $3, $4, now())', [entityType, entityId, imgName, imgPath], (error, result) => {
        if (error) {
            throw error
        }
        return true;
    })
}


async function getOrders(req, res, next) {  
    const { start, limit , statusIds , customerId} = req.body
    var orders = [];

    var query = `SELECT o.*, os.status_title, count(o.*) OVER() AS full_count from orders o 
    inner join order_status os on os.id = o.order_status_id`;

    if(customerId && statusIds && statusIds.length > 0){ 
        query = `${query} where o.customer_id =${customerId} and o.order_status_id in (${statusIds}) `
    } else if(customerId){
        query = `${query} where o.customer_id =${customerId} `
    } else if(statusIds && statusIds.length > 0){
        query = `${query} where o.order_status_id in (${statusIds}) `
    }  
    
    query = `${query} ORDER BY o.order_date DESC`
    
    if(_.has(req.body, "start") && _.has(req.body, "limit")){
        query = `${query} OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`;
    }

    pool.query(query)
    .then(function (results) {
        var totalCount = 0;
        if(results){
            _.each(results.rows, function(order) {
                var orderDetails = { 
                    orderId: order.id,
                    paymentMode: order.payment_mode, 
                    paymentStatus: order.payment_status,
                    transactionNum: order.transaction_num,
                    paymentDate: order.payment_date ? moment(order.payment_date).format('YYYY-MM-DD HH:mm:ss') : null,
                    orderStatus: order.status_title,
                    totalAmount: order.total_amount,
                    orderDate: moment(order.order_date).format('YYYY-MM-DD HH:mm:ss')
                }
    
                totalCount = order.full_count;
                orders.push(orderDetails);
            });
        }
        
        res.status(200).json({totalCount: totalCount, orders:orders})
    })
    .catch(function (err) {
        return next(err);
    });
}

async function removeOrder(req, res, next) { 
    const {id, statusId} = req.body; 
    pool.query("update orders SET order_status_id = $2, updated_on='now()' WHERE id = $1",[id, statusId])
    .then(function (result) {
        res.status(200).json({ 
            status: 'success', 
            message: 'Your order has been cancel successfully !!!'
        });
    })
    .catch(function (err) {
        return next(err);
    });
}

async function updateOrderStatus(req, res, next) { 
    const {id, statusId} = req.body; 
    pool.query("update orders SET order_status_id = $2, updated_on='now()' WHERE id = $1",[id, statusId])
    .then(function (result) {
        res.status(200).json({ 
            status: 'success', 
            message: 'Order status updated successfully. !!!'
        });
    })
    .catch(function (err) {
        return next(err);
    });
}

async function getOrderById(req, res, next) {  
    const id = parseInt(req.params.id);
    getOrderDetails(id, res, null, next);
}

async function getCustomerOrderById(req, res, next) {  
    const id = parseInt(req.params.id)
    const customerId = parseInt(req.params.customerId)
    getOrderDetails(id, res, customerId, next);
}



function getOrderDetails(id, response, customerId, next){
    var query = `SELECT o.*, os.status_title, sc.charges ,ofr.id "offerId", ofr.offer_desc, c.id "customerId", c.full_name "userName", c.mobile_no, ca.address, ca.city, ca.state, ca.pincode, ca.country  FROM orders o
                inner join order_status os on os.id = o.order_status_id
                inner join customer c on c.id = o.customer_id 
                inner join customer_address ca on ca.customer_id = c.id and ca.is_default = true
                left join offer ofr on ofr.id = o.offer_id
                left join shipping_charges sc on sc.id = o.shipping_charges_id                
                WHERE o.id = $1`
    
    pool.query(query, [id])
    .then(function (results) {
        var order = results.rows[0];
        var orderDetails = {};
        if(order){
            orderDetails = { 
                orderId: order.id,
                paymentMode: order.payment_mode, 
                paymentStatus: order.payment_status,
                transactionNum: order.transaction_num,
                paymentDate: order.payment_date ? moment(order.payment_date).format('YYYY-MM-DD HH:mm:ss') : null,
                orderStatus: order.status_title,     
                customerId: order.customerId,           
                customerName: order.userName,
                customerMobile: order.mobile_no,
                totalAmount: order.total_amount,
                shippingCharges: order.charges,
                offerId : order.offerId || null,
                offerDesc : order.offer_desc || "",
                orderDate: moment(order.order_date).format('YYYY-MM-DD HH:mm:ss'),
                frames: [],
                address : {
                    "address": order.address,
                    "city": order.city,
                    "state": order.state,
                    "pincode": order.pincode,
                    "country": order.country
                }
            }
            customerId = customerId ? customerId : order.customerId;
            getOrderItems(id, orderDetails, response, customerId, next)
        }
        
    })
    .catch(function (err) {
        return next(err);
    });

}


function getOrderItems(orderId, orderDetails, response, customerId, next){
    var query = `select f.id As "frameId", f.frame_code, f.frame_name, f.frame_desc, oi.price, oi.quantity, 
                oi.name_on_frame, oi.delivery_date, oi.occasion, i.img_name "frame_image_name", i.img_path "frame_image_path",
                ui.img_name "usr_choosed_img_name", ui.img_path "usr_choosed_img_path"
                from order_items oi
                inner join frame f on f.id = oi.frame_id
                left join images i on i.entity_id = f.id and i.entity_type = 'frame'
                left join images ui on ui.entity_id = oi.id and ui.entity_type = 'order'
                where oi.order_id = ${orderId}`;

    pool.query(query).then(function (results) {
        _.each(results.rows, function(orderItem, orderIndex) {
            var orderItemDetails = { 
                frameId : orderItem.frameId,
                frameCode : orderItem.frame_code, 
                frameName : orderItem.frame_name, 
                frameDesc : orderItem.frame_desc, 
                price : orderItem.price, 
                quantity : orderItem.quantity,
                nameOnFrame: orderItem.name_on_frame,
                deliveryDate: moment(orderItem.delivery_date).format('YYYY-MM-DD HH:mm:ss'),
                occasion: orderItem.occasion,
                frameImgName: orderItem.frame_image_name,
                frameImgPath: orderItem.frame_image_path,
                orderImgName: orderItem.usr_choosed_img_name,
                orderImgPath: orderItem.usr_choosed_img_path                
            }
            if(customerId){
                orderItemDetails['isAlreadyReview'] = false;
                orderItemDetails['review'] = {};
            }
            orderDetails.frames.push(orderItemDetails);
        });

        if(customerId){
            reviewCheck(customerId, orderDetails, response, next);
        } else {
            response.status(200).json(orderDetails);         
        }
    })
    .catch(function (err) {
        return next(err);
    });
}

function reviewCheck(customerId, orderDetails, response, next){
    var successCount = 0;
    _.each(orderDetails.frames, function(frame, orderIndex) {
        pool.query('select * from frame_review where frame_id=$1 and customer_id= $2', [frame.frameId, customerId])
        .then(function (isResult) {
            successCount = successCount +1;
            if(isResult.rowCount === 1){
               frame.isAlreadyReview = true;
               frame.review = {
                    id : isResult.rows[0].id,
                    rating: isResult.rows[0].rating,
                    review: isResult.rows[0].review
                }
            }

            if(orderDetails.frames.length === successCount){
                response.status(200).json(orderDetails);
            }
        })
        .catch(function (err) {
            return next(err);
        });
    });
    
}



module.exports = {
    createOrder,
    getOrders,
    getOrderById,
    removeOrder,
    updateOrderStatus,
    orderPayment,
    getCustomerOrderById
};