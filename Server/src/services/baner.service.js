var pool = require('../../_helpers/config');
var _ = require('underscore');
var moment = require('moment');

async function createBaner(req, res, next) {
    const { title, description } = JSON.parse(req.body.banerDetails);

    pool.query("INSERT INTO baner (title, baner_desc,created_on) VALUES ($1, $2,now()) RETURNING id", [title, description])
        .then(function(result) {
            if (req.file) {
                uploadImage(result.rows[0].id, req.file, res)
            }

            res.status(200).json({
                status: 'success',
                message: 'Added baner successfully !!!'
            });
        })
        .catch(function(err) {
            return next(err);
        });
}

async function getBaners(req, res, next) {
    var query = `SELECT b.id, b.title, b.baner_desc, i.img_name, i.img_path FROM baner b
    left join images i on i.entity_id = b.id and i.entity_type = $1 where b.is_active = $2 ORDER BY id ASC`;

    pool.query(query, ['baner', true])
        .then(function(result) {
            var baners = [];
            _.each(result.rows, function(baner) {
                var banerDetails = {
                    "id": baner.id,
                    "title": baner.title,
                    "description": baner.baner_desc,
                    "imageName": baner.img_name,
                    "imagePath": baner.img_path
                };
                baners.push(banerDetails);
            });
            res.status(200).json(baners);
        })
        .catch(function(err) {
            return next(err);
        });
}

async function removeBaner(req, res, next) {
    const id = parseInt(req.params.id);

    pool.query('update baner set is_active=$2 where id=$1', [id, false])
        .then(function(result) {
            res.status(200).json({
                status: 'success',
                message: 'Baner deleted successfully !!!'
            });
        }).catch(function(err) {
            return next(err);
        });
}

function uploadImage(banerId, file, res) {
    var entityType = 'baner',
        entityId = banerId,
        imgName = file.filename,
        imgPath = '/banerImg/' + file.filename;

    pool.query('INSERT INTO images (entity_type,entity_id,img_name, img_path,created_on) VALUES ($1, $2, $3, $4, now())', [entityType, entityId, imgName, imgPath], (error, result) => {
        if (error) {
            throw error
        }
        return true;
    })
}


module.exports = {
    createBaner,
    getBaners,
    removeBaner
};