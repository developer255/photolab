Server :
    This  is node + express js application with postgress database. Application support to autherization for api access.
    Node version is 10.15.3
    NPM version is 6.4.1
    psql (PostgreSQL) 10.12

Angular 8 : 
    Application build in angular 8 with authentication base. 

Setup steps:
    
    1) Execute following command under server and Admin portal folder
        
        npm install 

    2) Install postgressql 
    3) For run node application and setup database please check Server/README.md file
    4) After create new database please change data base name, username  and password in Server/_helpers/config.js
