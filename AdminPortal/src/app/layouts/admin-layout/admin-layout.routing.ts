import { Routes } from '@angular/router';
import { AuthGuard } from '../../_helpers/app.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { CategoryListComponent } from '../../category-list/category-list.component';
import { ProductListComponent } from '../../product-list/product-list.component';
import { OffersListComponent } from '../../offers-list/offers-list.component';
import { OrdersListComponent } from '../../orders-list/orders-list.component';
import { OrdersDetailComponent } from '../../orders-detail/orders-detail.component';
import { UserListComponent } from '../../users/user-list.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent},
    { path: 'category-list',     component: CategoryListComponent},
    { path: 'product-list',     component: ProductListComponent },
    { path: 'offers-list',     component: OffersListComponent },
    { path: 'orders-list',     component: OrdersListComponent },
    { path: 'orders-detail/:id',     component: OrdersDetailComponent },
    { path:  'user-list',        component: UserListComponent},
];
