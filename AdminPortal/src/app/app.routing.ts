import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './login/login.component';
// import { AdminComponentAdminComponent } from './admin/admin.component';
// import { AuthGuard } from './services/auth.guard';
import { AuthGuard } from './_helpers/app.routing';
import { DashboardComponent } from './dashboard/dashboard.component';


const routes: Routes =[
  
  //{ path: 'login', pathMatch: 'full', redirectTo: 'login'},
  { path: '', component: LoginComponent},
  { 
    path: 'dashboard',
    redirectTo: 'dashboard',
    pathMatch: 'full'
    
  }, {
    path: '',
    component: AdminLayoutComponent,
    children: [{
      path: '',
      loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
    }]
  },
  { path: '**', redirectTo: 'dashboard' }
];


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
       useHash: true
    })
  ],
  exports: [
  ],
})
/*
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})*/

export class AppRoutingModule { }
