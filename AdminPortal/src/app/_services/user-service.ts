import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { range } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { URL } from 'url';
import { User } from '../user';

const swal = require('sweetalert');
@Injectable({ providedIn: 'root' })

export class UserService {

    data: Object;
    loading: boolean;
    constructor(private http: Http) { }


    getUsers(data) {      
        return this.http.post('user/list', data)
          .map((response: Response) => {
            return response.json();
          }).catch(this.handleErrorObservable);
      }

  

    register(user: User) {
        return this.http.post('user/create', user)
        .map((response: Response) => {
          return response.json();
        }).catch(this.handleErrorObservable);
    }

    saveUser(formData) {
      return this.http.post('user/createAdmin', formData)
        .map((response: Response) => {
          return response.json();
        }).catch(this.handleErrorObservable);
    }
  

    deleteUser(id: number) {
        return this.http.delete('user/delete/'+id)
          .map((response: Response) => {
            return response.json();
          }).catch(this.handleErrorObservable);
      }


      updateUser(userDto) {
        return this.http.post('user/update', userDto)
          .map((response: Response) => {
            return response.json();
          }).catch(this.handleErrorObservable);
      }
    
      private handleErrorObservable(error: Response | any) {
        var res = JSON.parse(error._body)
        return swal(`Error`, `${res.error}`, 'error');
      }

}