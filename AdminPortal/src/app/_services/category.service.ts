import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { range } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { url } from 'inspector';

const swal = require('sweetalert');
@Injectable()
export class CategoryService {

  data: Object;
  loading: boolean;

  constructor(private http: HttpClient) { }

  getCategories() {
    var url = 'category/list';
    
    return this.http.post<any>(`${environment.apiUrl}/${url}`, {})
          .pipe(map(categories => {
            return categories;
          }));
  }

  saveCategory(formData) {
    return this.http.post('master/category/create', formData)
      .map((response: Response) => {
        return response.json();
      }).catch(this.handleErrorObservable);
  }


//   applyFilters(data) {
//     return this.http.post('stores/offers', data)
//       .map((response: Response) => {
//         return response.json();
//       }).catch(this.handleErrorObservable);
//   }

  private handleErrorObservable(error: Response | any) {
    var res = JSON.parse(error._body)
    return swal(`${res.status}`, `${res.message}`, 'error');
  }
}

