import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { range } from 'rxjs';
import { map, filter } from 'rxjs/operators';

const swal = require('sweetalert');
@Injectable()

export class OfferService {

    data: Object;
    loading: boolean;

    constructor(private http: HttpClient) { }

    //getProducts() {
       // var url = 'product/list';
        
     //   return this.http.get(url).map((response: Response) => {
       //     return response.json();
     //     }).catch(this.handleErrorObservable);
     // }

  getOffers(data) {
    return this.http.post<any>(`${environment.apiUrl}/offer/list`, data)
    .pipe(map(offers => {
        return offers;
      }))
   }

   saveOffer(formData) {
    return this.http.post<FormData>(`${environment.apiUrl}/offer/create`, formData)
    .pipe(map(offers => {
        return offers
      }));
  }

  deleteOffer(offerId) {
    return this.http.delete('offer/'+offerId).map((response: Response) => {
        return response.json();
      }).catch(this.handleErrorObservable);
   }

   updateOffer(offerObj){
     return this.http.post('offer/update',offerObj)
     .map((response: Response) =>{
       return response.json()
     }).catch(this.handleErrorObservable)
   }

private handleErrorObservable(error: Response | any) {
    var res = JSON.parse(error._body)
    return swal(`${res.status}`, `${res.message}`, 'error');
  }
}

