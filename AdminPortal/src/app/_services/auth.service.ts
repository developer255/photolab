import { Injectable } from '@angular/core';
// import { User } from '../user';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../_models/user';

const swal = require('sweetalert');
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    user_id = null;
    currentuser = null;
  
    constructor(private http: HttpClient) {
        if(!this.currentUserSubject){
          this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('loggedInUser')));
          this.currentUser = this.currentUserSubject.asObservable();
        }        
    }

    public get currentUserValue(): User {
      return this.currentUserSubject.value;
    }


  // constructor(private http: Http) { }

  login(username, password) {
      return this.http.post<any>(`${environment.apiUrl}/authorization/authenticate`, { username, password })
          .pipe(map(user => {
              // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
              user.token = window.btoa(user.userDetails.token);
              sessionStorage.setItem('loggedInUser', JSON.stringify(user.userDetails));
              this.currentUserSubject.next(user);
              return user;
          }));
  }
  
  public isLoggedIn(){
    return sessionStorage.getItem('loggedInUser') !== null;

  }

  public static getToken(){
    if(sessionStorage.getItem('loggedInUser')){
      var loggedInUser = JSON.parse(sessionStorage.getItem('loggedInUser'))
      return loggedInUser.token
    }
  }

  public logout(){
    sessionStorage.removeItem('loggedInUser');
    this.currentUserSubject.next(null);
  }

  private handleErrorObservable(error: Response | any) {
    var res = JSON.parse(error._body)
    return swal(`${res.status}`, `${res.message}`, 'error');
  }
}